﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static  Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("admin","123456");
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            var uid = userName.Text;
            var pwd = passWord.Text;

            var rest = _user.Where(x=>x.Key==uid ).FirstOrDefault();

            if (rest .Key ==uid && rest .Value==pwd )
            {
                MessageBox.Show("恭喜你登录成功","提示",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else 
            {
                MessageBox.Show("很遗憾登录失败","提示",MessageBoxButtons.YesNo,MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show();
        }
    }
}
