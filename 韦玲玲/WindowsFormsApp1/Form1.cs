﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //new的空的便于存储用户信息
        public static Dictionary<string, string> Users = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            Users.Add("aaa", "111");
        }

        //登录键
        private void button1_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = Users.Where(x=>x.Key==uid).FirstOrDefault();
            if(res.Key==uid && res.Value==pwd)
            {
                MessageBox.Show("登陆成功！","恭喜",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("登陆失败,请重新注册！","失败", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            }
           
        }

        //注册键
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }

        
    }
}
