﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication3;

namespace WindowsFormsApp1
{
    public partial class QQ登录 : Form
    {
        public QQ登录()
        {
            InitializeComponent();
        }

        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("wangyuexin", "15117387274");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if (res.Key == uid && res.Value == pwd)
            {
                MessageBox.Show("登录成功", "恭喜您!", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                MessageBox.Show("点击进入！");
            }
            else
            {
                MessageBox.Show("登录失败,请重新输入或注册一个账号", "很遗憾!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                MessageBox.Show("返回上一个界面！", "直接退出！");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RegisterForm regf = new RegisterForm();
            regf.Show();
        }

       
      
    }
}