﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformTest_Com_0001
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e) 
        {

        }
        
        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e) //方法
        {
            _user.Add("admin","123456");
        }
        /// <summary>
        /// 单击事件，点击窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> list = new List<Color>();   //List<Color>--泛型

            list.Add(Color.Black);

            list.Add(Color.White);

            list.AddRange(new Color[]
            {
                Color.Pink,Color.Blue,Color.Orange,Color.Yellow,Color.Green,Color.Cyan,Color.Purple,Color.Red
            });

            Random random = new Random();
            var rndInt = random.Next(0, list.Count);

            var rndColor = list[rndInt];

            BackColor = rndColor;

        }
        /// <summary>
        /// 双击事件，双击窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("SQL是结构化查询语言的缩写","提示");
        }

        private void Form1_BackColorChanged(object sender, EventArgs e)
        {
            MessageBox.Show("再次变色","变色龙系列");
        }

       // private void Form1_MouseDown(object sender, MouseEventArgs e)
       // {
         //   MessageBox.Show("鼠标按下时发生了一些事情");  
            
       // }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            MessageBox.Show("鼠标弹起时发生了另一些事情");
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if (res.Key==uid && res.Value==pwd)
            {
                MessageBox.Show("登录成功！", "提示",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else {
                MessageBox.Show("登录失败,您可以重新注册一个账号","提示",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            }
        }
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show();
        }
    }
}
