﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformTest_Com_0001
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRegister_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rPwd = reTryPassWord.Text;

            var isUidNull = string.IsNullOrEmpty(uid);
            var isPullNull = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == rPwd;

            if (!isUidNull && !isPullNull && isEqure)
            {
                MessageBox.Show("注册成功！", "提示");
                Form1._user.Add(uid,pwd);
                this.Close();
            }
            else {
                MessageBox.Show("注册未成功", "提示");
            }
        }
        /// <summary>
        /// 取消注册
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelRegister_Click(object sender, EventArgs e)
        {
            MessageBox.Show("GoodBye");
        }
    }
}
