﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormApp2
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void Register_Load(object sender, EventArgs e)
        {

        }

        private void btnRegiter_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rpwd = respassWord.Text;

            if (string.IsNullOrEmpty(uid) && string.IsNullOrEmpty(pwd) && pwd == rpwd)
            {
                MessageBox.Show("恭喜你，注册成功！");
                Form1._user.Add(uid, pwd);
            }
            else
            {
                MessageBox.Show("很抱歉，注册未成功！");
            }
        }
    }
}
