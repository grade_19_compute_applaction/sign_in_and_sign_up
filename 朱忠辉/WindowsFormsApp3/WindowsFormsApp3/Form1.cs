﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("nihao", "123456");
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> list = new List<Color>();
            list.Add(Color.Aqua);
            list.Add(Color.Black);
            list.AddRange(new Color[]
            {
                Color.Red,Color.Blue,Color.Gray,Color.Green,
                Color.Purple,Color.Yellow
            });
            Random random = new Random();
            var rndInt = random.Next(0, 8);
            var rndColor = list[rndInt];
            BackColor = rndColor;
        }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("双击鼠标发生的效果");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if(res.Key==uid && res.Value==pwd)
            {
                MessageBox.Show("登录成功", "恭喜你",MessageBoxButtons.YesNoCancel,MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("登陆失败,你可以重新注册一个 ", "很遗憾", MessageBoxButtons.YesNoCancel,MessageBoxIcon.Information);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
