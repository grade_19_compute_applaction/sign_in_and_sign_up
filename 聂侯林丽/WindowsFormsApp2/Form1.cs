﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("admin","123456");
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> list = new List<Color>();

            list.Add(Color.White);

            list.Add(Color.Black);

            list.AddRange(new Color[]
            {
              Color.Red,Color.Blue,Color.
              Green,Color.Orange,Color.Pink,Color.Yellow,Color.PowderBlue
            });

            Random random = new Random();
            var rndInt = random.Next(0, list.Count);

            var rndColor = list[rndInt];

            BackColor = rndColor;
        }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("你动我干啥，再动我试试?");
        }

        private void Form1_BackColorChanged(object sender, EventArgs e)
        {
            MessageBox.Show("你咋又变色了！","变色龙系列");
        }

       // private void Form1_MouseDown(object sender, MouseEventArgs e)
       // {
          //  MessageBox.Show("鼠标按下时，发生一些事情");
       // }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            MessageBox.Show("鼠标弹起来时，发生了另一些事情");
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if (res.Key==uid && res.Value==pwd) 
            {
                MessageBox.Show("登录成功","恭喜你",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("登录失败,你可以重新注册一个账号","请重新输入",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
