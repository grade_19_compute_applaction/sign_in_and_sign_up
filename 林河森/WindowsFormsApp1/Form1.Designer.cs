﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.UserName = new System.Windows.Forms.Label();
            this.PassWord = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.UserLogin = new System.Windows.Forms.Button();
            this.UserRegister = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UserName
            // 
            this.UserName.AutoSize = true;
            this.UserName.BackColor = System.Drawing.Color.Transparent;
            this.UserName.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.UserName.Location = new System.Drawing.Point(135, 104);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(142, 35);
            this.UserName.TabIndex = 10;
            this.UserName.Text = "用户名:";
            this.UserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PassWord
            // 
            this.PassWord.AutoSize = true;
            this.PassWord.BackColor = System.Drawing.Color.Transparent;
            this.PassWord.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold);
            this.PassWord.Location = new System.Drawing.Point(135, 151);
            this.PassWord.Name = "PassWord";
            this.PassWord.Size = new System.Drawing.Size(144, 35);
            this.PassWord.TabIndex = 9;
            this.PassWord.Text = "密  码:";
            this.PassWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("宋体", 21.75F);
            this.textBox1.Location = new System.Drawing.Point(283, 105);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(174, 34);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("宋体", 21.75F);
            this.textBox2.Location = new System.Drawing.Point(283, 152);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(174, 34);
            this.textBox2.TabIndex = 2;
            // 
            // UserLogin
            // 
            this.UserLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(105)))), ((int)(((byte)(172)))));
            this.UserLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.UserLogin.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UserLogin.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UserLogin.Location = new System.Drawing.Point(141, 212);
            this.UserLogin.Name = "UserLogin";
            this.UserLogin.Size = new System.Drawing.Size(118, 46);
            this.UserLogin.TabIndex = 3;
            this.UserLogin.Text = "登录";
            this.UserLogin.UseVisualStyleBackColor = false;
            this.UserLogin.Click += new System.EventHandler(this.UserLogin_Click);
            // 
            // UserRegister
            // 
            this.UserRegister.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(105)))), ((int)(((byte)(172)))));
            this.UserRegister.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UserRegister.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UserRegister.Location = new System.Drawing.Point(319, 212);
            this.UserRegister.Name = "UserRegister";
            this.UserRegister.Size = new System.Drawing.Size(117, 46);
            this.UserRegister.TabIndex = 4;
            this.UserRegister.Text = "注册";
            this.UserRegister.UseVisualStyleBackColor = false;
            this.UserRegister.Click += new System.EventHandler(this.UserRegister_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(586, 318);
            this.Controls.Add(this.UserLogin);
            this.Controls.Add(this.UserRegister);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.PassWord);
            this.Controls.Add(this.UserName);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "登录界面";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UserName;
        private System.Windows.Forms.Label PassWord;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button UserLogin;
        private System.Windows.Forms.Button UserRegister;
    }
}

