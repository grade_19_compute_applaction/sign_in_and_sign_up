﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_Register
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        //声明Dictionnary对象用于存储数据
        public static Dictionary<string, string> _user = new Dictionary<string, string>();
        

        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("admin", "123");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string userName = this.userName.Text;
            string userPwd = this.userPwd.Text;
            var res = _user.Where(x => x.Key == userName).FirstOrDefault();
            if (userName == res.Key)
            {
                if (userPwd == res.Value)
                {
                    MessageBox.Show("登录成功！","提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("密码错误！登陆失败！","警告！",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(userName))
                {
                    DialogResult dres = MessageBox.Show("用户名不存在！是否现在注册一个账号？", "错误", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dres == DialogResult.Yes)
                    {
                        Register user = new Register();
                        user.ShowDialog();
                    }
                    else
                    {
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("用户名不能为空！","警告",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
        }

        private void 注册_Click(object sender, EventArgs e)
        {
            Register newUser = new Register();
            newUser.ShowDialog();
        }

    }
}
