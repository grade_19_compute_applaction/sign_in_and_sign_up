﻿namespace WindowsFormsApp1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserName = new System.Windows.Forms.Label();
            this.UserPwd = new System.Windows.Forms.Label();
            this.RUserPwd = new System.Windows.Forms.Label();
            this.username1 = new System.Windows.Forms.ComboBox();
            this.userpwd1 = new System.Windows.Forms.ComboBox();
            this.ruserpwd1 = new System.Windows.Forms.ComboBox();
            this.register = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UserName
            // 
            this.UserName.AutoSize = true;
            this.UserName.Location = new System.Drawing.Point(205, 63);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(53, 12);
            this.UserName.TabIndex = 0;
            this.UserName.Text = "用户名：";
            this.UserName.Click += new System.EventHandler(this.label1_Click);
            // 
            // UserPwd
            // 
            this.UserPwd.AutoSize = true;
            this.UserPwd.Location = new System.Drawing.Point(205, 108);
            this.UserPwd.Name = "UserPwd";
            this.UserPwd.Size = new System.Drawing.Size(41, 12);
            this.UserPwd.TabIndex = 0;
            this.UserPwd.Text = "密码：";
            // 
            // RUserPwd
            // 
            this.RUserPwd.AutoSize = true;
            this.RUserPwd.Location = new System.Drawing.Point(205, 158);
            this.RUserPwd.Name = "RUserPwd";
            this.RUserPwd.Size = new System.Drawing.Size(65, 12);
            this.RUserPwd.TabIndex = 0;
            this.RUserPwd.Text = "确认密码：";
            // 
            // username1
            // 
            this.username1.FormattingEnabled = true;
            this.username1.Location = new System.Drawing.Point(317, 60);
            this.username1.Name = "username1";
            this.username1.Size = new System.Drawing.Size(121, 20);
            this.username1.TabIndex = 1;
            // 
            // userpwd1
            // 
            this.userpwd1.FormattingEnabled = true;
            this.userpwd1.Location = new System.Drawing.Point(317, 105);
            this.userpwd1.Name = "userpwd1";
            this.userpwd1.Size = new System.Drawing.Size(121, 20);
            this.userpwd1.TabIndex = 1;
            // 
            // ruserpwd1
            // 
            this.ruserpwd1.FormattingEnabled = true;
            this.ruserpwd1.Location = new System.Drawing.Point(317, 150);
            this.ruserpwd1.Name = "ruserpwd1";
            this.ruserpwd1.Size = new System.Drawing.Size(121, 20);
            this.ruserpwd1.TabIndex = 1;
            // 
            // register
            // 
            this.register.Location = new System.Drawing.Point(266, 213);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(117, 29);
            this.register.TabIndex = 2;
            this.register.Text = "注册";
            this.register.UseVisualStyleBackColor = true;
            this.register.Click += new System.EventHandler(this.register_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.register);
            this.Controls.Add(this.ruserpwd1);
            this.Controls.Add(this.userpwd1);
            this.Controls.Add(this.username1);
            this.Controls.Add(this.RUserPwd);
            this.Controls.Add(this.UserPwd);
            this.Controls.Add(this.UserName);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UserName;
        private System.Windows.Forms.Label UserPwd;
        private System.Windows.Forms.Label RUserPwd;
        private System.Windows.Forms.ComboBox username1;
        private System.Windows.Forms.ComboBox userpwd1;
        private System.Windows.Forms.ComboBox ruserpwd1;
        private System.Windows.Forms.Button register;
    }
}