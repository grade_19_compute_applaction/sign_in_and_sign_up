﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForm
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rPwd = reTrypassWord.Text;

            var isUidNull = string.IsNullOrEmpty(uid);
            var isPullNull = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == rPwd;

            if(! isUidNull && ! isPullNull && isEqure)
            {
                MessageBox.Show("注册成功","恭喜您", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                LoginForm._user.Add(uid,pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册未成功,请返回重新注册", "很抱歉", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }

        }

         private void btnCancelRegister_Click(object sender, EventArgs e)
        {
            MessageBox.Show("GoodBye");
        }
    }
}
