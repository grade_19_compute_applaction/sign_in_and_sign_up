﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = uname.Text;
            var pwd = pword.Text;
            var rpwd = rpword.Text;

            var a = string.IsNullOrEmpty(uid);
            var b = string.IsNullOrEmpty(pwd);
            var c = pwd == rpwd;

            if(!a && !b && c)
            {
                MessageBox.Show("注册成功！");
                Form1.Users.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败，请重试！");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
