﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loadform
{
    public partial class Form1 : Form
    {
        public static Dictionary<string, string> User = new Dictionary<string, string>();

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //设置初始登录账号密码
            User.Add("lsq", "123");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //获取文本信息
            //Uid=lsq
            //Pwd=123

            var Uid = uName.Text;
            var Pwd = uPwd.Text;
            //判断是否能够登录
            var load = User.Where(x => x.Key == Uid).FirstOrDefault();
            if (load.Key == Uid && load.Value == Pwd)
            {
                //登录成功
                MessageBox.Show("恭喜你，登录成功！", "登录成功", MessageBoxButtons.YesNo);
            }
            else
            {
                //登录失败
                MessageBox.Show("登录失败，请重新登录！", "登录失败", MessageBoxButtons.YesNo);
            }
        }

        //跳转到注册窗体
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
